namespace :integration do
  namespace :foundation do
    namespace :images do
      image_files = []
      Dir.glob('DataTablesPlugins/integration/foundation/images/*.png') do |src_file|
        src_filename = File.basename(src_file)
        tgt_file = "app/assets/images/datatables/integration/foundation/#{src_filename}"

        file tgt_file => src_file do
          cp src_file, tgt_file
        end

        image_files.push tgt_file
      end

      desc "Delete images"
      task :clean do
        Dir.glob('app/assets/images/datatables/integration/foundation/*.png') do |file|
          File.delete(file)
        end
      end

      desc "Copy images from DataTablesPlugins/integration/foundation/images"
      task copy: image_files

      desc "Setup image assets"
      task setup: [:clean, :copy]
    end

    namespace :javascripts do
      src_file = "DataTablesPlugins/integration/foundation/dataTables.foundation.js"
      tgt_file = "app/assets/javascripts/datatables/integration/foundation/datatables_foundation.js"
      file tgt_file => src_file do
          cp src_file, tgt_file
      end

      desc "Copy #{src_file}"
      task copy: tgt_file

      desc "Setup javascript assets"
      task setup: :copy
    end

    namespace :stylesheets do
      src_file = "DataTablesPlugins/integration/foundation/dataTables.foundation.css"
      tgt_file = "app/assets/stylesheets/datatables/integration/foundation/datatables_foundation.css.scss"
      file tgt_file => src_file do
          cp src_file, tgt_file
      end

      desc "Copy #{src_file}"
      task copy: tgt_file

      desc "Fix image URLs in stylesheets"
      task :fix_urls do
        content = File.read(tgt_file)
        fixed_content = content.gsub(/url\(\'images\/([A-Za-z_]*.png)\'\)/, 'image-url("datatables/integration/foundation/\1")')
        File.open(tgt_file, "w") { |f| f.puts fixed_content}
      end

      desc "Setup stylesheet assets"
      task setup: [:copy, :fix_urls]
    end

    desc "Setup integration/foundation asset files"
    task setup: ["images:setup", "javascripts:setup", "stylesheets:setup"]
  end
end
