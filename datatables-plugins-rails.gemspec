# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "datatables-plugins-rails"
  spec.version       = "1.10.7.0"
  spec.authors       = ["Sascha Vincent Kurowski"]
  spec.email         = ["svkurowski@gmail.com"]
  spec.summary       = %q{Write a short summary. Required.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = Dir["{lib,app}/**/*"] + ["LICENSE.txt", "README.md"]
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
